# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Copyright 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

BASE_URL="https://matt.ucc.asn.au/dropbear"
HOMEPAGE="${BASE_URL}/dropbear.html"
DOWNLOADS="${BASE_URL}/releases/${PNV}.tar.bz2"

SUMMARY="Small but featureful secure shell server and client, designed for low-memory environments"

SLOT="0"
LICENCES="BSD-2 BSD-3 MIT public-domain"

PLATFORMS="~amd64 ~armv7 ~armv8"

MYOPTIONS="pam xinetd"

DEPENDENCIES="
    build+run:
        pam?    ( sys-libs/pam )
        xinetd? ( sys-apps/xinetd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # NOTE(somasis) I couldn't get it to build with external libtom libraries
    --enable-bundled-libtom
    --disable-plugin
    --disable-fuzz
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( pam )
DEFAULT_SRC_COMPILE_PARAMS=( MULTI=1 )
DEFAULT_SRC_INSTALL_PARAMS=( MULTI=1 )
DEFAULT_SRC_INSTALL_EXCLUDE=( release.sh )

disable_define() {
    edo sed -i -e "/^#define ${1}/d" options.h
}

enable_define() {
    edo sed -i -e "/#define ${1}/"'{s:^/\*::;s:\*/$::}' options.h
}

src_configure() {
    default

    edo sed -i -r 's/(LIBS\+=.*)/\1 -ldl/g' Makefile

    enable_define DROPBEAR_CLI_ASKPASS_HELPER

    if option pam;then
        enable_define DROPBEAR_SVR_PAM_AUTH
        disable_define DROPBEAR_SVR_PASSWORD_AUTH
    fi
    option xinetd || disable_define INETD_MODE

    edo sed \
            -e 's|/var/run|/run|g'                 \
            -e "s|/bin|/$(exhost --target)/bin|"   \
            -i options.h
}

src_install() {
    default

    keepdir /etc/dropbear
}

