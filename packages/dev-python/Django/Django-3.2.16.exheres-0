# Copyright 2018 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require pypi
# python_opts=[sqlite] only for tests
require setup-py [ import=setuptools blacklist='2' python_opts="[sqlite]" ]

SUMMARY="A high-level Python Web framework"

LICENCES="BSD-3"
SLOT="2"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/asgiref[>=3.3.2&<4][python_abis:*(-)?]
        dev-python/pytz[python_abis:*(-)?]
        dev-python/sqlparse[>=0.2.2][python_abis:*(-)?]
    test:
        dev-python/aiosmtpd[python_abis:*(-)?]
        dev-python/bcrypt[python_abis:*(-)?]
        dev-python/docutils[python_abis:*(-)?]
        dev-python/Jinja2[>=2.9.2][python_abis:*(-)?]
        dev-python/numpy[python_abis:*(-)?]
        dev-python/Pillow[>=6.2.0][python_abis:*(-)?]
        dev-python/PyYAML[python_abis:*(-)?]
        dev-python/tblib[>=1.5.0][python_abis:*(-)?]
        sys-devel/gettext
"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    # Wants to bind to 0.0.0.0@0
    edo sed -e 's/test_specified_port_bind/_&/' -i tests/servers/tests.py

    # TODO: Find out why this fails
    edo sed -e 's/test_custom_fields/_&/' -i tests/inspectdb/tests.py

    if [[ $(python_get_abi) == 3.11 ]] ; then
        # The tests below fail with 3.11, maybe we should blacklist 3.11?
        edo sed \
            -e 's/test_skip_if_db_feature/_&/' \
            -e 's/test_skip_unless_db_feature/_&/' \
            -i tests/test_utils/tests.py
        edo sed -e 's/test_add_failing_subtests/_&/' \
            -i tests/test_runner/test_parallel.py
        edo sed -e 's/test_output_verbose/_&/' \
            -i tests/test_runner/test_debug_sql.py
    fi
}

test_one_multibuild() {
    PYTHONPATH=. edo ${PYTHON} -B tests/runtests.py \
        --parallel ${EXJOBS:-1} \
        --verbosity=2
}

